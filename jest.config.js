module.exports = {
  rootDir: '.',
  moduleFileExtensions: ['js', 'json',],
  transformIgnorePatterns: ['node_modules'],
  testEnvironment: 'jest-environment-jsdom',
  moduleNameMapper: {
    '^@components(.*)$': '<rootDir>/src/components$1',
    '^@composed-components(.*)$': '<rootDir>/src/composed-components$1',
    '^@features(.*)$': '<rootDir>/src/features$1',
  },
  testRegex: '.*\\.test\\.js$',
  transform: {
    "^.+\\.js$": "babel-jest"
  },
};
