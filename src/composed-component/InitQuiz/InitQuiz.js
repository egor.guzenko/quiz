import { Button } from 'antd';
import QuizInput from '@components/QuizInput/QuizInput';



const InitQuiz = ({ quizMethods: { changeQuizOption, setCorrectValue }, quizValues }) => {
  return (
    <div className="init-quiz">
      {quizValues.map((_, index) => (
        <QuizInput
          className="quiz-input"
          key={index}
          onChange={setCorrectValue}
          idx={index}
          placeholder="0"
        />
      ))}
      <Button
        className="quiz-confirm"
        onClick={changeQuizOption}
      >Confirm</Button>
    </div>
  );
};

export default InitQuiz;
