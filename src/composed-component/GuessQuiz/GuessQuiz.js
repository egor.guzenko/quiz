import QuizInput from '@components/QuizInput/QuizInput';

const GuessQuiz = ({ quizMethods: { handleAnswers }, quizValues, enable }) => {
  return (
    <div className="guess-quiz">
      {quizValues.map((_, idx) => (
        <QuizInput
          className="quiz-input"
          key={idx}
          onChange={handleAnswers}
          idx={idx}
          disabled={enable !== idx}
        />
      ))}
    </div>
  );
};

export default GuessQuiz;
