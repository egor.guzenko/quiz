import { Button } from 'antd';

const QuizRestart = ({ quizMethods: { quizRestart } }) => {
  return (
    <Button
      className="quiz-restart"
      onClick={quizRestart}
    >Restart Game</Button>
  );
}

export default QuizRestart;
