import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Layout } from 'antd';
import {
  selectCorrectValue,
  selectEnable,
  changeCurrentEnable,
  selectOption,
  selectWin,
  quizRestart,
  setCorrectValue,
  changeQuizOption
} from '@features/quiz/quizSlice';
import InitQuiz from './composed-component/InitQuiz/InitQuiz';
import GuessQuiz from './composed-component/GuessQuiz/GuessQuiz';
import QuizRestart from './composed-component/QuizRestart/QuizRestart';
import './App.css';

const { Content, Header } = Layout;

function App() {
  const dispatch = useDispatch();
  const enable = useSelector(selectEnable);
  const quizValues = useSelector(selectCorrectValue);
  const quizOption = useSelector(selectOption);
  const isWin = useSelector(selectWin);
  const quizMethods = useMemo(() => ({
    setCorrectValue: (value, idx) => dispatch(setCorrectValue({ 
        value: Number(value),
        idx,
    })),
    handleAnswers: (value, idx) => {
      const isCorrect = value && quizValues[idx] === Number(value);

      if (!isCorrect) {
        return;
      }
    
      dispatch(changeCurrentEnable());
    },
    changeQuizOption: () => dispatch(changeQuizOption()),
    quizRestart: () => dispatch(quizRestart())
  }));

  return (
    <Layout>
      <Content className="quiz-wrapper">
        {quizOption ?
          <GuessQuiz quizMethods={quizMethods} enable={enable} quizValues={quizValues}/> :
          <InitQuiz quizMethods={quizMethods} quizValues={quizValues}/> 
        }
        {!isWin || <QuizRestart quizMethods={quizMethods}/>}
      </Content>
    </Layout>
  );
}

export default App;
