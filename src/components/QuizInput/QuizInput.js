import React from 'react';
import { Input } from 'antd';

const QuizInput = ({ className, placeholder="", disabled=false, onChange=() => {}, idx }) => {
  return (
    <Input
      className={className}
      type="number"
      onChange={({ target }) => onChange(target.value, idx)}
      placeholder={placeholder}
      disabled={disabled}
      aria-label="quiz-input"
    />
  );
}

export default React.memo(QuizInput);
