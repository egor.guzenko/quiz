import React from 'react';
import { screen, render, fireEvent } from '@testing-library/react';
import QuizInput from './QuizInput';

describe('test for QuizInput', () => {
  test('should get disabled property by props', () => {
    render(
      <QuizInput disabled={true} />
    );

    const input = screen.getByLabelText('quiz-input');

    expect(input.disabled).toBe(true);
  });

  test('should get onChange property by props', () => {
    const onChangeHandler = jest.fn();

    render(
      <QuizInput onChange={onChangeHandler} />
    );

    const input = screen.getByLabelText('quiz-input');

    fireEvent.change(input, { target: { value: '5' }});

    expect(onChangeHandler).toBeCalledTimes(1);
  });

  test('should pass id prop as a second argument in onChange function', () => {
    const onChangeHandler = jest.fn();

    render(
      <QuizInput idx={5} onChange={onChangeHandler} />
    );

    const input = screen.getByLabelText('quiz-input');

    fireEvent.change(input, { target: { value: '5' }});

    expect(onChangeHandler).toHaveBeenCalledWith('5', 5);
  });
});
