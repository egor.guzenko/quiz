import { configureStore } from '@reduxjs/toolkit';
import quizReducer, {
  setCorrectValue,
  changeCurrentEnable,
  changeQuizOption,
  quizRestart
} from './quizSlice';

export const store = configureStore({
  reducer: {
    quiz: quizReducer,
  },
});

describe('tests for quiz slice', () => {
  describe('tests for setCorrectValue', () => {
    it('should change value of specified input', () => {
      const payload = {
        value: 3,
        idx: 0,
      };

      store.dispatch(setCorrectValue(payload));
      expect(store.getState().quiz.quizCorrectValues[0]).toBe(3);
    });
  });
  
  describe('tests for changeCurrentEnable', () => {
    it('should change current enable value', () => {
      expect(store.getState().quiz.currentEnable).toBe(0);
      store.dispatch(changeCurrentEnable());
      expect(store.getState().quiz.currentEnable).toBe(1);
    });

    it('should change isWin to opposite value if function reached last element', () => {
      store.dispatch(changeCurrentEnable());
      store.dispatch(changeCurrentEnable());
      store.dispatch(changeCurrentEnable());
      store.dispatch(changeCurrentEnable());
      expect(store.getState().quiz.currentEnable).toBe(5);
      expect(store.getState().quiz.isWin).toBe(true);
    })
  });

  describe('tests for changeQuizOption', () => {
    it('should change quizOption to opposite value', () => {
      expect(store.getState().quiz.quizOption).toBe(false);
      store.dispatch(changeQuizOption());
      expect(store.getState().quiz.quizOption).toBe(true);
    });
  });


  describe('tests for quizRestart', () => {
    it('should reset game to initial state', () => {
      const payload = {
        value: 3,
        idx: 0,
      };

      store.dispatch(setCorrectValue(payload));
      expect(store.getState().quiz.quizCorrectValues[0]).toBe(3);
      store.dispatch(quizRestart());
      expect(store.getState().quiz.quizCorrectValues[0]).toBe(0);
    })
  })
});
