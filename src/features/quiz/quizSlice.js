import { createSlice } from '@reduxjs/toolkit';
import reducers from './reducers';

const AMOUNT_INPUTS = 5;
export const initialState = {
  quizCorrectValues: new Array(AMOUNT_INPUTS).fill(0),
  currentEnable: 0,
  quizOption: false,
  isWin: false,
};

const quizSlice = createSlice({
  name: 'quizSlice',
  initialState,
  reducers
});

export const { setCorrectValue, changeCurrentEnable, changeQuizOption, quizRestart } = quizSlice.actions;
export const selectCorrectValue = ({ quiz }) => quiz.quizCorrectValues;
export const selectEnable = ({ quiz }) => quiz.currentEnable;
export const selectOption = ({ quiz }) => quiz.quizOption;
export const selectWin = ({ quiz }) => quiz.isWin;
export default quizSlice.reducer;
