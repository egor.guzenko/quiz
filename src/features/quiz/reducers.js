import { initialState } from './quizSlice';

const reducers = {
  setCorrectValue: (state, { payload: { value, idx } }) => {
    const newQuizCorrectValues = [...state.quizCorrectValues];

    newQuizCorrectValues[idx] = value;

    return { ...state, quizCorrectValues: newQuizCorrectValues };
  },
  changeCurrentEnable: state => {
    let { currentEnable, quizCorrectValues, isWin } = state;

    if (currentEnable === quizCorrectValues.length - 1) {
      isWin = !isWin;
    }
    
    currentEnable += 1;

    return { ...state, currentEnable, isWin };
  },
  changeQuizOption: state => {
    const { quizOption } = state;

    return { ...state, quizOption: !quizOption };
  },
  quizRestart: () => initialState,
}

export default reducers;
