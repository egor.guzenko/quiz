const { alias } = require('react-app-rewire-alias');

module.exports = function override(config) {
  alias({
    '@components': 'src/components',
    '@features': 'src/features',
    '@composed-component': 'src/composed-component'
  })(config);

  return config;
};
